<?php

use app\models\User;
use yii\db\Migration;

/**
 * Class m200805_092205_add_user
 */
class m200805_092205_add_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $user = new User();
        $user->name = 'admin';
        $user->login = 'admin';
        $user->setPassword('admin');
        $user->save();

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200805_092205_add_user cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200805_092205_add_user cannot be reverted.\n";

        return false;
    }
    */
}
